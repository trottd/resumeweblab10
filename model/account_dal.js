var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view account_view as
 select s.*, a.street, a.zip_code from account s
 join skill a on a.skill_id = s.skill_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT c.* FROM account c ' +
        'LEFT JOIN account_skill ca on ca.account_id = c.account_id ' +
        'LEFT JOIN skill a on a.skill_id = ca.skill_id ' +
        'WHERE c.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (email, first_name, last_name) VALUES (?, ?, ?)';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE account_ID RETURNED AS insertId AND THE SELECTED skill_IDs INTO account_skill
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountskillData = [];
        if (params.skill_id.constructor === Array) {
            for (var i = 0; i < params.skill_id.length; i++) {
                accountskillData.push([account_id, params.skill_id[i]]);
            }
        }
        else {
            accountskillData.push([account_id, params.skill_id]);
        }

        // NOTE THE EXTRA [] AROUND accountskillData
        connection.query(query, [accountskillData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var accountskillInsert = function(account_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountskillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            accountskillData.push([account_id, skillIdArray[i]]);
        }
    }
    else {
        accountskillData.push([account_id, skillIdArray]);
    }
    connection.query(query, [accountskillData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountskillInsert = accountskillInsert;

//declare the function so it can be used locally
var accountskillDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_skill WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountskillDeleteAll = accountskillDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE account SET email = ?, first_name = ?, last_name = ? WHERE account_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        //delete account_skill entries for this account
        accountskillDeleteAll(params.account_id, function(err, result){

            if(params.skill_id != null) {
                //insert account_skill ids
                accountskillInsert(params.account_id, params.skill_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used here
 DROP PROCEDURE IF EXISTS account_getinfo;

 DELIMITER //
 CREATE PROCEDURE account_getinfo (account_id int)
 BEGIN

 SELECT * FROM account WHERE account_id = _account_id;

 SELECT a.*, s.account_id FROM skill a
 LEFT JOIN account_skill s on s.skill_id = a.skill_id AND account_id = _account_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL account_getinfo (4);

 */

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};