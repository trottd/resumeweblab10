var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view resume_view as
 select s.* from resume s
 join account a on a.account_id = s.account_id;

 */

exports.getAll = function(callback) {
    var query = 'select resume_id, resume_name, first_name, last_name from resume '+
        'join account on account.account_id = resume.account_id order by first_name';


    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAll2 = function(callback) {
    var query = 'select resume_id, resume_name, first_name, last_name from resume '+
        'join account on account.account_id = resume.account_id order by first_name';


    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'SELECT r.*, sc.school_name, co.company_name, sk.skill_name FROM resume r ' +
        'left join resume_school s on s.resume_id = r.resume_id ' +
        'left join school sc on sc.school_id = s.school_id ' +
        'left join resume_company c on c.resume_id = r.resume_id ' +
        'left join company co on co.company_id = c.company_id ' +
        'left join resume_skill k on k.resume_id = r.resume_id ' +
        'left join skill sk on sk.skill_id = k.skill_id ' +
        'where r.resume_id = ?';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE resume
    var query = 'INSERT INTO resume (account_id) VALUES (?)';

    var QueryData = [params.account_id];

    connection.query(query, QueryData, function(err, result) {
            callback(err, result);
        });
};

exports.insert2 = function(params, callback) {

    // FIRST INSERT THE resume
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?, ?)';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE resume_ID RETURNED AS insertId AND THE SELECTED account_IDs INTO resume_account
        var resume_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

        var resumeschoolData = [];
        for (var i = 0; i < params.school_id.length; i++) {
            resumeschoolData.push([resume_id, params.school_id[i]]);
        }

        // NOTE THE EXTRA [] AROUND resumeaccountData
        connection.query(query, [resumeschoolData], function(err, result){

            var query2 = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';
            var resumecompanyData = [];
            for (var j = 0; j < params.company_id.length; j++) {
                resumecompanyData.push([resume_id, params.company_id[j]]);
            }
            // NOTE THE EXTRA [] AROUND resumeaccountData
            connection.query(query2, [resumecompanyData], function(err, result){

                var query3 = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
                // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
                var resumeskillData = [];
                for (var k = 0; k < params.skill_id.length; k++) {
                    resumeskillData.push([resume_id, params.skill_id[k]]);
                }


                // NOTE THE EXTRA [] AROUND resumeaccountData
                connection.query(query3, [resumeskillData], function(err, result){
                    callback(err, result);
                });

                //callback(err, result);
            });

//            callback(err, result);
        });




    });

};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.getAccount = function(callback) {
    var query = 'select * from account' +
        ' order by first_name';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

//declare the function so it can be used locally
var resumeInsertAll = function(resume_id, accountIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_account (resume_id, account_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeaccountData = [];
    if (accountIdArray.constructor === Array) {
        for (var i = 0; i < accountIdArray.length; i++) {
            resumeaccountData.push([resume_id, accountIdArray[i]]);
        }
    }
    else {
        resumeaccountData.push([resume_id, accountIdArray]);
    }
    connection.query(query, [resumeaccountData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeInsertAll = resumeInsertAll;

//declare the function so it can be used locally
var resumeDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_account WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeDeleteAll = resumeDeleteAll;

exports.update = function(params, callback) {

    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [params.resume_id];

    connection.query(query, queryData, function(err, result) {
        var query = 'DELETE FROM resume_company WHERE resume_id = ?';
        var queryData = [params.resume_id];

        connection.query(query, queryData, function(err, result) {
            var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
            var queryData = [params.resume_id];

            connection.query(query, queryData, function(err, result) {

                    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

                    var resumeschoolData = [];
                    for (var i = 0; i < params.school_id.length; i++) {
                        resumeschoolData.push([params.resume_id, params.school_id[i]]);
                    }

                    // NOTE THE EXTRA [] AROUND resumeaccountData
                    connection.query(query, [resumeschoolData], function(err, result){

                        var query2 = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';
                        var resumecompanyData = [];
                        for (var j = 0; j < params.company_id.length; j++) {
                            resumecompanyData.push([params.resume_id, params.company_id[j]]);
                        }
                        // NOTE THE EXTRA [] AROUND resumeaccountData
                        connection.query(query2, [resumecompanyData], function(err, result){

                            var query3 = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
                            // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
                            var resumeskillData = [];
                            for (var k = 0; k < params.skill_id.length; k++) {
                                resumeskillData.push([params.resume_id, params.skill_id[k]]);
                            }


                            // NOTE THE EXTRA [] AROUND resumeaccountData
                            connection.query(query3, [resumeskillData], function(err, result){
                                callback(err, result);
                            });

                            //callback(err, result);
                        });

//            callback(err, result);
                    });




                });

                //callback(err, result);
            });
        });


};

/*  Stored procedure used here
 DROP PROCEDURE IF EXISTS resume_getinfo;

 DELIMITER //
 CREATE PROCEDURE resume_getinfo (resume_id int)
 BEGIN

 SELECT * FROM resume WHERE resume_id = _resume_id;

 SELECT a.*, s.resume_id FROM account a
 LEFT JOIN resume_account s on s.account_id = a.account_id AND resume_id = _resume_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL resume_getinfo (4);

 */

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit2 = function(account_id, callback) {
    var query = 'CALL account_getaccountinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};