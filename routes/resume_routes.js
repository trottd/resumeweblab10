var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');

router.get('/success', function(req, res) {
    res.render('resume/resumeSuccess');
});


// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result': result});
        }
    });

});

// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeViewById', {'result': result});
            }
        });
    }
});

// Return the add a new resume form
router.get('/add/selectuser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getAccount(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd', {'result': result});
        }
    });
});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd2', {account: result[0][0], school: result[1], company: result[2], skill: result[3]});
        }
    });
});

// View the resume for the given id
router.get('/insert2', function(req, res){

    // passing all the query parameters (req.query) to the insert function instead of each individually
    if(req.query.school_id == null) {
        res.send('school must be provided.');
    }
    else if(req.query.company_id == null) {
        res.send('At least one company must be selected');
    }
    else if(req.query.skill_id == null) {
        res.send('At least one skill must be selected');
    }
    else {
        resume_dal.insert2(req.query, function (err, result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {


                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/success');
            }
        });
    }
});

router.post('/insert', function(req, res){
    // simple validation
    if(req.body.account_id == null) {
        res.send('At least one account must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.edit2(req.body.account_id, function(err, result){
            res.render('resume/resumeAdd2', {account: result[0][0], school: result[1], company: result[2], skill: result[3]});
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0][0], school: result[1], company: result[2], skill: result[3]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.resume_name == null) {
        res.send('An resume_name is required');
    }
    else {
        resume_dal.getById(req.query.resume_name, function(err, resume){
            account_dal.getAll(function(err, account) {
                res.render('resume/resumeUpdate', {resume: resume[0], account: account});
            });
        });
    }

});

router.post('/update', function(req, res) {
        resume_dal.update(req.body, function(err, result){
            res.redirect(302, '/resume/success');
        })
});

// Delete a resume for the given resume_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});

module.exports = router;
